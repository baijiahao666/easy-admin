package com.mars.module.system.service;

import com.mars.common.response.sys.CommonDataResponse;

/**
 * @author mars
 * @version 1.0
 * @date 2023/10/31 22:26
 */
public interface ICommonDataService {

    /**
     * 获取系统公共数据
     */
    CommonDataResponse acquire();


}
