package com.mars.module.tool.utils;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * 功能描述
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2024-01-18 11:09:49
 */
public class SyslogSender {

    private static final int SYSLOG_PORT = 514;

    public static void main(String[] args) {
        String syslogServerAddress = "127.0.0.1"; // Syslog服务器的IP地址
        String message = "Hello, Syslog!"; // 要发送的消息内容

        try (DatagramSocket socket = new DatagramSocket()) {
            InetAddress serverAddress = InetAddress.getByName(syslogServerAddress);

            byte[] messageBytes = message.getBytes();
            DatagramPacket packet = new DatagramPacket(messageBytes, messageBytes.length, serverAddress, SYSLOG_PORT);
            socket.send(packet);

            System.out.println("Message sent to Syslog server.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
