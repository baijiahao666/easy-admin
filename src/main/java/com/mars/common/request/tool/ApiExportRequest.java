package com.mars.common.request.tool;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Api导出DTO
 *
 * @author 源码字节-程序员Mars
 */
@Data
public class ApiExportRequest {

    @ApiModelProperty(value = "swagger地址（如：http://localhost:9900）")
    private String swaggerUrl;

    @NotNull
    @ApiModelProperty(value = "接口地址集合")
    private List<String> urlList;

}
