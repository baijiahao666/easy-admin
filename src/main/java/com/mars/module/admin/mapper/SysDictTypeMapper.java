package com.mars.module.admin.mapper;

import com.mars.module.admin.entity.SysDictType;
import com.mars.framework.mapper.BasePlusMapper;

/**
 * 字典类型Mapper接口
 *
 * @author mars
 * @date 2023-11-18
 */
public interface SysDictTypeMapper extends BasePlusMapper<SysDictType> {

}
